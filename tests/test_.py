from webapp import create_app
###################
#### DISCLAMER ####
###################

# Normally I would create separete 
# test cases for unit test, and functional 
# tests. In bigger project an examplar structure
# of tests could look ex. like that: 
#       ├── tests
#       │   ├── functional
#       │   │   ├── __init__.py
#       │   │   └── test_users.py
#       │   └── unit
#       │       ├── __init__.py
#       │       └── test_app.py
# However, this recruitment problem
# doesn't require such testing, there are
# only two endpoints to test. I haven't included 
# any tests for sharedstate.SharedState, as it is  
# a textbook example of how to use BaseManager module.
# furthermore I have decided to create two simple tests in one file. 

def test_predict():
    app = create_app()

    with app.test_client() as test_client:
        response = test_client.post('/predict', json = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        assert response.status_code == 200
        response = test_client.post('/predict', json = [1, 2, 3])
        assert response.status_code == 400

def test_metric():
    app = create_app()

    with app.test_client() as test_client:
        response = test_client.get('/metrics')
        assert response.status_code == 200
        assert float(response.json['Metric']) == 0