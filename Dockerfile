# syntax=docker/dockerfile:1

FROM python:3.7.4-slim-buster

WORKDIR /app

COPY . .

# setting up system for xgboost and installing all requierments
RUN apt-get update && apt install -y libssl-dev libffi-dev libgomp1
RUN pip install --upgrade pip && pip install -r requierments.txt

# starting flask app
ENV FLASK_APP="webapp:create_app()"
CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]