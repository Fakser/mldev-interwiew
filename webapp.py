from flask import Flask, jsonify, request
from predict import Predict
from sharedstate import SharedState
from time import time
from datetime import datetime
from logging.config import dictConfig



def create_app(config_filename = None): 
    """Function creating Flask application.

    Args:
        config_filename (string, optional): Path to the configuration file https://flask.palletsprojects.com/en/0.12.x/config/. Defaults to None.

    Returns:
        object: Flask application
    """

    dictConfig({ # Definition of logging configuration dict.
        "version": 1,
        "disable_existing_loggers": True,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
            },
            "access": {
                "format": "%(message)s",
            }
        },
        "handlers": {
            "console": {
                "level": "INFO",
                "class": "logging.StreamHandler",
                "formatter": "default",
                "stream": "ext://sys.stdout",
            },
        },
        "root": {
            "level": "INFO",
            "handlers": ["console"],
        }
    })

    app = Flask(__name__) # Definition of Flask app
    if config_filename != None:
        app.config.from_pyfile(config_filename)
    ss = SharedState(('127.0.0.1', 35792), b'secret') # Global shared variable for storing traffic on '/predict' endpoint. 
    ss.traffic = 0 # TTraffic for current app is set to 0. This value will be shared and editable between processes/endpoint.

    predictor = Predict() # Definition of Predict object, which stores xgboost model and uses it for predictions
    app_start_time = time() # Definition of start time for metric calculating avarage number of requests per second
    # Required endpoints:

    @app.route('/predict', methods=['GET', 'POST'])
    def predict():
        # the endpoint has to take n previous values of the traffic to predict the new one
        ss.traffic += 1
        try: 
            values = request.json
            prediction = predictor.predict(datetime.now(), values)
        except Exception as e:
            return str(e), 400
        return jsonify({'Prediction': str(prediction)}), 200


    @app.route('/metrics', methods=['GET'])
    def metrics():
        return jsonify({'Metric': ss.traffic/(time() - app_start_time)}), 200   # 'Request per seconds to the /predict endpoint'

    return app

