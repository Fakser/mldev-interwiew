# My solution

## Approach to the task

For the web framework I've choosen Flask. It is probably the best library to develop simple rest-api systems.
Flask app is created inside of a function create_app(). It is one of the best methods for creating Flask applications, as with such approach we can 
easly test them, and even create multiple instances of them. For logging I have decided to use console handler, as docker container stores stdout (and stderr if needed),
which can be easly extracted from the container. As it was mentioned in the recruitment task, I was supposed to develop two endpoints: /predict and /metric. 
The /metric endpoint should return avarage requests per second on /predict endpoint. Because of that there was a need to create some structure that stores this traffic.
This variable should be editable between multiple threads (as flask handles requests with multithreadding). One approach to this problem is to initialize a database with table "traffic",
and each time /predict is used by a client, it updates this table. Although during my interview I was asked about updating a global variable inside multithreadded code. 
That is why I have decided to use BaseHandler from multithreadding library. Basehandler is an Object interface, that can be wrappet into a class. In my case it is SharedState, 
which is a textbook example from the doccumentation. 

## Running docker container
 $ cd {PROJECT_PATH}/<br>
 $ docker build --tag cobrick-mldev-interview .<br>
 $ docker run -d -p 80:80 cobrick-mldev-interview<br>
## ML Dev interview

The web service predicts traffic (requests per second) in the next time window based on the n previous time window traffic values.

# Description

The repository contains the following files:

* prediction.model: simple XGBRegressor model. For usage, for example, see predict.py file.
* predict.py: a class simple model to predict the traffic. This is an example code to load and use the prediction.model file.
* webapp.py: example Flask application with endpoint definition.

# Requirenments:

1. The application must be delivered in the Python language.

2. The Python version libraries are to the candidate choice.

3. The application should be delivered as a Pull Request to this git repository or by creating fork.

4. The application should contain basic logging and tests.

5. The application must expose 2 endpoints:

/predict
  - in:  JSON array with a list of previous values 
  - out: predicted value. The prediction has to be calculated by the model loaded from the prediction.model file.

/metrics
  - out: one metric requests per second on /predict endpoint

6. The candidate has to deliver a script or command to create a runnable docker container.

